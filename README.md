
# Responsive Image Gallery #

## What you have to do? ##

* You have to develop a Responsive Image Gallery.
* It's required to develop version for large and small screens.
    * Check the images in the assets folder
* On clicking next button the next image should be loaded, selected and shown.
* On clicking prev button the previous image should be loaded, selected and shown
* The selected image thumbnail should be highlighted from the rest using a white border.

## Hints ##

* Start with mobile first but keep in mind 2 columnar grid structure
* Make use of standard semantic html tags to build the page structure.
* Make use of CSS3 media queries  and use just one break point to optimize for desktop and smaller devices. Desktop and iPad should have same view and anything smaller than iPad should be mobile version.
* Keep an eye on the details and difference in the desktop and mobile version.
* Upload your page on a web server to test it using Responsinator or make use of Responsive layout which comes with Firefox/Chrome web developer tools to test your pages in different screen locally.
* Test your page in Internet Explorer 10 and up, Chrome and Firefox.